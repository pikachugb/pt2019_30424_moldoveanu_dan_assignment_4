package presentation;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionListener;
import java.util.Map;
import java.util.TreeMap;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

import business.MenuItem;

public class WaiterView extends JPanel{
	//gbc
    private GridBagConstraints c = new GridBagConstraints();
    //items
	private JComboBox<MenuItem> menuItemsComboBox;
	private JComboBox<MenuItem> currentItemsComboBox;
	
	private JButton addItemButton = new JButton("Add product to order");
	private JButton removeItemButton = new JButton("Remove product from order");
	private JButton placeOrderButton = new JButton("Place order");

	private JLabel addItemLabel = new JLabel("Choose product to add to the order: ");
	private JLabel removeItemLabel = new JLabel("Choose product to remove from order: ");
	private JLabel computePriceLabel = new JLabel("Price: 0 RON");
	private ActionListener actionListener;
	
	public void generateItems() {
		c.insets = new Insets(5, 5, 5, 5);
		//0
		c.gridx = 0;
		c.gridy = 0;
		this.add(addItemLabel,c);
		c.gridx = 1;
		c.gridy = 0;
		menuItemsComboBox.setPreferredSize(new Dimension(180,30));
		this.add(menuItemsComboBox,c);
		c.gridx = 2;
		c.gridy = 0;
		addItemButton.addActionListener(actionListener);
		this.add(addItemButton,c);
		
		//1
		c.gridx = 0;
		c.gridy = 1;
		this.add(removeItemLabel,c);
		c.gridx = 1;
		c.gridy = 1;
		currentItemsComboBox.setPreferredSize(new Dimension(180,30));
		this.add(currentItemsComboBox,c);
		c.gridx = 2;
		c.gridy = 1;
		removeItemButton.addActionListener(actionListener);
		this.add(removeItemButton,c);
		
		//2
		c.gridx = 1;
		c.gridy = 2;
		this.add(computePriceLabel, c);
		
		//3
		c.gridx = 1;
		c.gridy = 3;
		c.fill = GridBagConstraints.BOTH;
		placeOrderButton.addActionListener(actionListener);
		this.add(placeOrderButton,c);
	}
	
	public void populateMenuItemsComboBox(TreeMap<Integer, MenuItem> menuItemsMap) {
		menuItemsComboBox.removeAllItems();
		for(Map.Entry<Integer, MenuItem> menuItem: menuItemsMap.entrySet()) 
			menuItemsComboBox.addItem(menuItem.getValue());		
	}
	
	public WaiterView(ActionListener actionListener) {
		this.actionListener = actionListener;
		menuItemsComboBox = new JComboBox<MenuItem>();
		currentItemsComboBox = new JComboBox<MenuItem>();
		this.setLayout(new GridBagLayout());
		generateItems();
	}

	public JComboBox<MenuItem> getMenuItemsComboBox() {
		return menuItemsComboBox;
	}

	public JComboBox<MenuItem> getCurrentItemsComboBox() {
		return currentItemsComboBox;
	}

	public JButton getAddItemButton() {
		return addItemButton;
	}

	public JButton getRemoveItemButton() {
		return removeItemButton;
	}

	public JButton getPlaceOrderButton() {
		return placeOrderButton;
	}

	public JLabel getComputePriceLabel() {
		return computePriceLabel;
	}
}
