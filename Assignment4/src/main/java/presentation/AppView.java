package presentation;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

public class AppView extends JFrame {
	private JTabbedPane tabbedPane;
	
	public AppView(JPanel p1, JPanel p2, JPanel p3) {
		tabbedPane = new JTabbedPane();
		tabbedPane.addTab("Administrator", null, p1, null);
		tabbedPane.addTab("Waiter", null, p2, null);
		tabbedPane.addTab("Chef", null, p3, null);
		this.add(tabbedPane);
		
		this.setTitle("Restaurant");
		this.setSize(700,400);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLocationRelativeTo(null);
	}
}
