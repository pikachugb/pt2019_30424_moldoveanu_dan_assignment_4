package presentation;

import java.util.ArrayList;
import java.util.HashMap;

import business.MenuItem;
import business.Order;

public interface Observer {
	void update(HashMap<Order, ArrayList<MenuItem>> ordersMap);
}
