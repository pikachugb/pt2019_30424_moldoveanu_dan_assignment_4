package presentation;

import java.awt.Dimension;
import java.awt.GridBagLayout;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import business.MenuItem;
import business.Order;
import business.Restaurant;

import presentation.Observer;

public class ChefView extends JPanel implements Observer{
	private JTable ordersTable;
	private JScrollPane scrollPane;
	public ChefView(Restaurant restaurant) {
		this.setLayout(new GridBagLayout());
        fillOrdersTable(restaurant.getOrders());
	}
	
	public void fillOrdersTable(HashMap<Order, ArrayList<MenuItem>> ordersMap) {
		this.removeAll();
		
		String[] col = {"Order ID", "Date", "Table", "Items"};
		int i = 0;	
		String[][] data = new String[ordersMap.size()][4];
		for (Order order: ordersMap.keySet()) {
			data[i][0] = "" + order.getOrderID();
			data[i][3] = "";
			for (MenuItem menuItem: ordersMap.get(order)) 
				data[i][3] += menuItem.getName() + " | ";
			data[i][1] = order.getDate().toString();
			data[i][2] = String.valueOf(order.getTable());
			i++;
		}
		
		ordersTable = new JTable(data,col);
		ordersTable.getColumnModel().getColumn(0).setPreferredWidth(25);
		ordersTable.getColumnModel().getColumn(1).setPreferredWidth(110);
		ordersTable.getColumnModel().getColumn(2).setPreferredWidth(25);
		ordersTable.getColumnModel().getColumn(3).setPreferredWidth(240);
		scrollPane = new JScrollPane(ordersTable);
		scrollPane.setPreferredSize(new Dimension(500,300));
		
		this.add(scrollPane);
	}
	
	public void update(HashMap<Order, ArrayList<MenuItem>> ordersMap) {	
		fillOrdersTable(ordersMap);
	}

}
