package presentation;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionListener;
import java.util.Map;
import java.util.TreeMap;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import business.MenuItem;

public class AdminView extends JPanel{
	//gbc
    private GridBagConstraints c = new GridBagConstraints();
    //items
    private JLabel priceLabel = new JLabel("Price:");
	private JTextField nameField = new JTextField();
	private JTextField priceField = new JTextField();
	private JTextField editField = new JTextField();
	private JComboBox<MenuItem> menuItemsComboBox = new JComboBox<MenuItem>();
	private JComboBox<MenuItem> editComboBox = new JComboBox<MenuItem>();
	private JComboBox<MenuItem> removeComboBox = new JComboBox<MenuItem>();
	private JButton addBaseProductButton = new JButton("Add base product");
	private JButton addSubProductButton = new JButton("Add");
	private JButton addCompositeProductButton = new JButton("Add composite product");
	private JButton editItemButton = new JButton("Edit item");
	private JButton removeItemButton = new JButton("Remove item");
	private ActionListener actionListener;

    private void generateItems() {
    	c.insets = new Insets(5, 5, 5, 5);
    	//0
		c.gridx = 0;
		c.gridy = 0;
		nameField.setPreferredSize(new Dimension(250, 30));
		this.add(nameField, c);
		c.gridx = 1;
		c.gridy = 0;
		this.add(priceLabel,c);
		c.gridx = 2;
		c.gridy = 0;
		priceField.setPreferredSize(new Dimension(100, 30));
		this.add(priceField, c);
		c.gridx = 3;
		c.gridy = 0;
		addBaseProductButton.addActionListener(actionListener);
		this.add(addBaseProductButton, c);
		//1
		c.gridx = 0;
		c.gridy = 1;
		menuItemsComboBox.setPreferredSize(new Dimension(250, 30));
		this.add(menuItemsComboBox, c);
		c.gridx = 1;
		c.gridy = 1;
		addSubProductButton.addActionListener(actionListener);
		this.add(addSubProductButton, c);
		c.gridx = 2;
		c.gridy = 1;
		c.gridwidth = 2;
		addCompositeProductButton.addActionListener(actionListener);
		this.add(addCompositeProductButton, c);
		//2
		c.gridx = 1;
		c.gridy = 2;
		c.gridwidth = 1;
		editField.setPreferredSize(new Dimension(120, 30));
		this.add(editField, c);
		c.gridx = 0;
		c.gridy = 2;
		editComboBox.setPreferredSize(new Dimension(250, 30));
		this.add(editComboBox, c);
		c.gridx = 2;
		c.gridy = 2;
		editItemButton.addActionListener(actionListener);
		this.add(editItemButton, c);
		//3
		c.gridx = 0;
		c.gridy = 3;
		this.add(removeComboBox, c);
		c.gridx = 1;
		c.gridy = 3;
		removeComboBox.setPreferredSize(new Dimension(250, 30));
		removeItemButton.addActionListener(actionListener);
		this.add(removeItemButton, c);
	}
	
	public void populateComboBox(TreeMap<Integer, MenuItem> menuItemsMap) {
		menuItemsComboBox.removeAllItems();
		for(Map.Entry<Integer, MenuItem> menuItem: menuItemsMap.entrySet()) 
			menuItemsComboBox.addItem(menuItem.getValue());		
	}
	
	public AdminView(ActionListener actionListener) {
		this.actionListener = actionListener;
		this.setLayout(new GridBagLayout());
		generateItems();
	}

	public JTextField getNameField() {
		return nameField;
	}

	public JComboBox<MenuItem> getMenuItemsComboBox() {
		return menuItemsComboBox;
	}

	public JButton getAddBaseProductButton() {
		return addBaseProductButton;
	}

	public JButton getAddSubProductButton() {
		return addSubProductButton;
	}

	public JButton getAddCompositeProductButton() {
		return addCompositeProductButton;
	}

	public JTextField getPriceField() {
		return priceField;
	}

	public JTextField getEditField() {
		return editField;
	}

	public JComboBox<MenuItem> getEditComboBox() {
		return editComboBox;
	}

	public JComboBox<MenuItem> getRemoveComboBox() {
		return removeComboBox;
	}

	public JButton getEditItemButton() {
		return editItemButton;
	}

	public JButton getRemoveItemButton() {
		return removeItemButton;
	}
}
