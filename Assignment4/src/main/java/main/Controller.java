package main;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JOptionPane;

import business.BaseProduct;
import business.CompositeProduct;
import business.MenuItem;
import business.Restaurant;
import presentation.AdminView;
import presentation.AppView;
import presentation.ChefView;
import presentation.Observer;
import presentation.WaiterView;

public class Controller {
	Restaurant restaurant;
	private AdminView adminView;
	private WaiterView waiterView;
	private ChefView chefView;
	private AppView appView;
	private String subProductName = "";
	private String path = "";
	private float currentPrice = 0;
	
	private ArrayList<MenuItem> compositeItems = new ArrayList<MenuItem>();
	
	private void updateComboBoxes() {
		waiterView.getMenuItemsComboBox().removeAllItems();
		adminView.getMenuItemsComboBox().removeAllItems();
		adminView.getEditComboBox().removeAllItems();
		adminView.getRemoveComboBox().removeAllItems();

		for (MenuItem menuItem: restaurant.getMenu()) {
			waiterView.getMenuItemsComboBox().addItem(menuItem);
			adminView.getMenuItemsComboBox().addItem(menuItem);
			adminView.getEditComboBox().addItem(menuItem);
			adminView.getRemoveComboBox().addItem(menuItem);
		}
	}
	
	private ActionListener adminActionListener = new ActionListener() {
		
		public void actionPerformed(ActionEvent e) {
			JButton sourceButton = (JButton) e.getSource();
			
			int price = 0;
			
			//add base product
			if (sourceButton == adminView.getAddBaseProductButton()) {
				try {
					price = Integer.parseInt(adminView.getPriceField().getText());
					
					BaseProduct baseProduct = new BaseProduct(adminView.getNameField().getText(), price);
					restaurant.addMenuItemToMenu(baseProduct);
					updateComboBoxes();
					restaurant.saveMenu(path + "\\menu\\menu.txt");
					System.out.println("Menu saved!");
					adminView.getNameField().setText("");
					adminView.getPriceField().setText("");
				}
				catch (NumberFormatException exception) {
					JOptionPane.showMessageDialog(null, "Introduce only numbers for the price!", "Input error", JOptionPane.ERROR_MESSAGE);
				}
			}
			//add subproduct
			if (sourceButton == adminView.getAddSubProductButton()) {
				MenuItem menuItem = (MenuItem) adminView.getMenuItemsComboBox().getSelectedItem();
				if (menuItem != null) {
					compositeItems.add(menuItem);
					subProductName += menuItem.getName() + "; ";
				}
			}
			//add composite
			if (sourceButton == adminView.getAddCompositeProductButton()) {
				CompositeProduct compositeProduct = new CompositeProduct(subProductName,compositeItems);
				restaurant.addMenuItemToMenu(compositeProduct);
				subProductName = "";
				updateComboBoxes();
				restaurant.saveMenu(path + "\\menu\\menu.txt");
				System.out.println("Menu saved!");
			}
			//edit item 
			if (sourceButton == adminView.getEditItemButton()) {
				restaurant.editMenuItemFromMenu((MenuItem)adminView.getEditComboBox().getSelectedItem(), restaurant.getMenu(), adminView.getEditField().getText());
				updateComboBoxes();
				restaurant.saveMenu(path + "\\menu\\menu.txt");
			}
			//remove 
			if (sourceButton == adminView.getRemoveItemButton()) {
				restaurant.removeMenuItemFromMenu((MenuItem)adminView.getRemoveComboBox().getSelectedItem(), restaurant.getMenu());
				updateComboBoxes();
				restaurant.saveMenu(path + "\\menu\\menu.txt");
			}
		}
	};

	private ActionListener waiterActionListener = new ActionListener() {
		
		public void actionPerformed(ActionEvent e) {
			JButton sourceButton = (JButton) e.getSource();
			
			//add item
			if (sourceButton == waiterView.getAddItemButton()) {
				waiterView.getCurrentItemsComboBox().addItem((MenuItem)waiterView.getMenuItemsComboBox().getSelectedItem());
				currentPrice += ((MenuItem)waiterView.getMenuItemsComboBox().getSelectedItem()).computePrice();
				waiterView.getComputePriceLabel().setText("Price: " + currentPrice + " RON");
			}
			//remove item
			if (sourceButton == waiterView.getRemoveItemButton()) {
				if (currentPrice != 0)
					currentPrice -= ((MenuItem)waiterView.getCurrentItemsComboBox().getSelectedItem()).computePrice();
				waiterView.getComputePriceLabel().setText("Price: " + currentPrice + " RON");
				waiterView.getCurrentItemsComboBox().removeItem(waiterView.getCurrentItemsComboBox().getSelectedItem());
			}
			//place order 
			if (sourceButton == waiterView.getPlaceOrderButton()) {
				if (waiterView.getCurrentItemsComboBox().getItemCount() != 0) {
					ArrayList<MenuItem> currentOrderList = new ArrayList<MenuItem>();
					while (waiterView.getCurrentItemsComboBox().getItemCount() != 0) {
						currentOrderList.add(waiterView.getCurrentItemsComboBox().getItemAt(0));
						waiterView.getCurrentItemsComboBox().removeItemAt(0);
					}
					restaurant.addOrder(currentOrderList);
					restaurant.printBill(restaurant.getCurrentOrder(),path);
					waiterView.getComputePriceLabel().setText("Price: 0 RON");
					currentPrice = 0;
				}
			}
		}
	};
	
	private void loadMenu() {
		try {
			path = new File(".").getCanonicalPath();
			
			restaurant.loadMenu(path + "\\menu\\menu.txt");
			System.out.println("Menu loaded!");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public Controller() {
		restaurant = new Restaurant();
		adminView = new AdminView(adminActionListener);
		waiterView = new WaiterView(waiterActionListener);
		chefView = new ChefView(restaurant);
		appView = new AppView(adminView, waiterView, chefView);
		appView.setVisible(true);
		restaurant.addObserver((Observer) chefView);
		loadMenu();
		updateComboBoxes();
	}
	
	public static void main(String[] args) {
		new Controller();
	}
}
