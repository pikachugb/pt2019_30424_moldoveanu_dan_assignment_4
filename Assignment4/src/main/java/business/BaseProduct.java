package business;

public class BaseProduct extends MenuItem {
	private float price;
	
	public BaseProduct(String name, float price) {
		super(name);
		this.price = price;
	}
	
	public float computePrice() {
		return this.price;
	}
}
