package business;

import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import business.Observable;

public class Restaurant extends Observable implements RestaurantProcessing{
	private ArrayList<MenuItem> menu;
	private HashMap<Order, ArrayList<MenuItem>> orders;
	private Order currentOrder;
	
	public Restaurant() {
		menu = new ArrayList<MenuItem>();
		orders = new HashMap<Order,ArrayList<MenuItem>>();
		currentOrder = new Order(orders.size() + 1, 0);
	}
	
	public void addMenuItemToMenu(MenuItem menuItem) {
		assert menuItem != null;
		menu.add(menuItem);		
	}
	
	public void removeMenuItemFromMenu(MenuItem menuItem, ArrayList<MenuItem> items) {
		assert menuItem != null;
		assert items.size() != 0;
		items.remove(menuItem);
	}
	
	public void editMenuItemFromMenu(MenuItem menuItem, ArrayList<MenuItem> items, String newName) {
		assert menuItem != null;
		assert items.size() != 0;
		assert newName != null;
		items.get(items.indexOf(menuItem)).setName(newName);
	}
	
	public void addOrder(ArrayList<MenuItem> items) {
		assert items.size() != 0;
		Random random = new Random();
		
		currentOrder = new Order(orders.size() + 1, random.nextInt(30));
		for (MenuItem menuItem: items)
			currentOrder.addMenuItem(menuItem);
		
		orders.put(currentOrder, items);
		this.notifyObservers(orders);
	}
	
	public void saveMenu(String fileName) {
		try {
			assert fileName != null;
			FileOutputStream outputStream = new FileOutputStream(fileName);
			ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream);
			
			for (MenuItem menuItem: menu)
				objectOutputStream.writeObject(menuItem);
			
			objectOutputStream.close();
			outputStream.close();			
		} catch (IOException e) {
			e.printStackTrace();
		}	
	}
	
	public void loadMenu(String fileName) {
		try {
			assert fileName != null;
			FileInputStream fileInputStream = new FileInputStream(fileName);
			ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
			
			MenuItem menuItem;
			while((menuItem = (MenuItem) objectInputStream.readObject()) != null)
				menu.add(menuItem);
			
			objectInputStream.close();
			fileInputStream.close();
		} catch (IOException e) {
			//e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	public float getOrderPrice(Order order) {
		assert order != null;
		float price = 0;
		for (MenuItem menuItem: orders.get(order)) 
			price += menuItem.computePrice();
		return price;
	}
	
	public void printBill(Order order, String path) {
		try {
			assert order != null;
			assert path != null;
			
			BufferedWriter writer; 
			String text = "";
			
			text += order.toString();
			text += "\nTOTAL: " + getOrderPrice(order) + "RON";
			
			writer = new BufferedWriter(new FileWriter(path + "\\bills\\order" + order.getOrderID() + ".txt"));
			writer.write(text);
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public ArrayList<MenuItem> getMenu() {
		return menu;
	}

	public HashMap<Order, ArrayList<MenuItem>> getOrders() {
		return orders;
	}

	public Order getCurrentOrder() {
		return currentOrder;
	}
}
