package business;

import java.io.Serializable;

public class MenuItem implements Serializable {
	private String name;
	
	public MenuItem(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
	
	public float computePrice() {
		return 0; 
	}
	
	public String toString() {
		return name + " - " + computePrice() + " RON";
	}
	
	public void setName(String name) {
		this.name = name;
	}
}
