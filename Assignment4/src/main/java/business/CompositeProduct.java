package business;

import java.util.ArrayList;

public class CompositeProduct extends MenuItem{
	private ArrayList<MenuItem> menuItems;
	
	public CompositeProduct(String name, ArrayList<MenuItem> menuItems) {
		super(name);
		this.menuItems = menuItems;
	}
	
	public void addMenuItem(MenuItem menuItem) {
		menuItems.add(menuItem);
	}
	
	public float computePrice() {
		float price = 0;
		for(MenuItem menuItem: menuItems) 
			price += menuItem.computePrice();
		return price;
	}
}
