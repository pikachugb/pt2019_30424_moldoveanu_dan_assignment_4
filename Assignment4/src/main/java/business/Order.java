package business;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class Order {
	private int orderID;
	private Date date;
	private int table;
	private ArrayList<MenuItem> menuItems;
	
	public Order(int orderID, int table) {
		menuItems = new ArrayList<MenuItem>();
		this.orderID = orderID;
		this.table = table;
		this.date = Calendar.getInstance().getTime();
	}
	
	public Order(Order order) {
		this.date = order.getDate();
		this.orderID = order.getOrderID();
		this.table = order.getTable();
		this.menuItems = order.getMenuItems();
	}
	
	public void addMenuItem(MenuItem menuItem) {
		menuItems.add(menuItem);
	}
	
	public String toString() {
		String string = "";
		string += "Order ID: " + orderID;
		string += "\n------------------------------------------------";
		string += "\nTable: " + table;
		string += "\n------------------------------------------------";
		string += "\nDate: " + date.toString();
		string += "\n------------------------------------------------";
		string += "\nMenu items:\n";
		for (MenuItem menuItem: menuItems)
			string += "  " + menuItem.toString() + "\n";
		string += "------------------------------------------------";
		return string;
	}
	
	@Override
	public int hashCode() {
		return orderID;
	}
	
	@Override 
	public boolean equals(Object o) {
		if (this == o)
			return true;
		Order order = (Order)o;
		if(order.getDate() == this.getDate() && order.table == this.table && order.getOrderID() == this.orderID)
			return true;
		return false;
	}

	public int getOrderID() {
		return orderID;
	}

	public void setOrderID(int orderID) {
		this.orderID = orderID;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public int getTable() {
		return table;
	}

	public void setTable(int table) {
		this.table = table;
	}

	public ArrayList<MenuItem> getMenuItems() {
		return menuItems;
	}

	public void setMenuItems(ArrayList<MenuItem> menuItems) {
		this.menuItems = menuItems;
	}
}
