package business;

import java.util.ArrayList;
import java.util.HashMap;

import presentation.Observer;

public class Observable {
	private ArrayList<Observer> observers = new ArrayList<Observer>();
	
	public void notifyObservers(HashMap<Order, ArrayList<MenuItem>> ordersMap) {
		for(Observer observer: observers) 
			observer.update(ordersMap);
	}
	
	public void addObserver(Observer observer) {
		observers.add(observer);
	}	
}
