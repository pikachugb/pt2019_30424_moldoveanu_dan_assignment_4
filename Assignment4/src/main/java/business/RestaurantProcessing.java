package business;

import java.util.ArrayList;

public interface RestaurantProcessing {
	/**
	 * @pre menuItem != null
	 * @post @nochange
	 */
	void addMenuItemToMenu(MenuItem menuItem);
	
	/**
	 * @pre items.size() != 0
	 * @post @nochange
	 */
	void addOrder(ArrayList<MenuItem> items);
	
	/**
	 * @pre fileName != null
	 * @post @nochange
	 */
	void saveMenu(String fileName);
	
	/**
	 * @pre fileName != null
	 * @post @nochange
	 */
	void loadMenu(String fileName);
	
	/**
	 * @pre order != null
	 * @post @nochange
	 */
	public float getOrderPrice(Order order);
	
	/**
	 * @pre order != null && path != null
	 * @post @nochange
	 */
	public void printBill(Order order, String path);
	
	/**
	 * @pre menuItem != null && items.size() != 0
	 * @post
	 */
	public void removeMenuItemFromMenu(MenuItem menuItem, ArrayList<MenuItem> items);
}
